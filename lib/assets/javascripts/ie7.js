/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referring to this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-home': '&#xe636;',
		'icon-home2': '&#xe623;',
		'icon-camera': '&#xe624;',
		'icon-play': '&#xe625;',
		'icon-bullhorn': '&#xe637;',
		'icon-cart': '&#xe638;',
		'icon-cart2': '&#xe639;',
		'icon-cart3': '&#xe63a;',
		'icon-print': '&#xe63b;',
		'icon-user': '&#xe602;',
		'icon-users': '&#xe603;',
		'icon-zoomin': '&#xe63c;',
		'icon-zoomout': '&#xe63d;',
		'icon-lock': '&#xe63e;',
		'icon-unlocked': '&#xe63f;',
		'icon-remove': '&#xe640;',
		'icon-truck': '&#xe641;',
		'icon-powercord': '&#xe642;',
		'icon-list': '&#xe600;',
		'icon-list2': '&#xe601;',
		'icon-attachment': '&#xe643;',
		'icon-thumbs-up': '&#xe644;',
		'icon-thumbs-up2': '&#xe645;',
		'icon-enter': '&#xe646;',
		'icon-exit': '&#xe647;',
		'icon-mail': '&#xe632;',
		'icon-mail2': '&#xe633;',
		'icon-googleplus': '&#xe604;',
		'icon-googleplus2': '&#xe605;',
		'icon-googleplus3': '&#xe626;',
		'icon-facebook': '&#xe606;',
		'icon-twitter': '&#xe627;',
		'icon-twitter2': '&#xe607;',
		'icon-twitter3': '&#xe628;',
		'icon-feed': '&#xe608;',
		'icon-feed2': '&#xe609;',
		'icon-vimeo2': '&#xe60a;',
		'icon-vimeo': '&#xe60b;',
		'icon-github': '&#xe629;',
		'icon-github2': '&#xe62a;',
		'icon-github3': '&#xe60c;',
		'icon-github22': '&#xe60d;',
		'icon-blogger': '&#xe62b;',
		'icon-blogger2': '&#xe60e;',
		'icon-yahoo': '&#xe62c;',
		'icon-tux': '&#xe62d;',
		'icon-apple': '&#xe60f;',
		'icon-finder': '&#xe62e;',
		'icon-android': '&#xe610;',
		'icon-windows8': '&#xe62f;',
		'icon-skype': '&#xe630;',
		'icon-linkedin': '&#xe611;',
		'icon-pinterest': '&#xe612;',
		'icon-pinterest2': '&#xe613;',
		'icon-xing': '&#xe631;',
		'icon-file-pdf': '&#xe648;',
		'icon-file-word': '&#xe649;',
		'icon-file-excel': '&#xe64a;',
		'icon-phone': '&#xe614;',
		'icon-user2': '&#xe615;',
		'icon-users2': '&#xe616;',
		'icon-user-add': '&#xe617;',
		'icon-search': '&#xe618;',
		'icon-tools': '&#xe619;',
		'icon-camera2': '&#xe61a;',
		'icon-lock2': '&#xe61b;',
		'icon-lock-open': '&#xe61c;',
		'icon-logout': '&#xe61d;',
		'icon-login': '&#xe61e;',
		'icon-minus': '&#xe634;',
		'icon-plus': '&#xe635;',
		'icon-list3': '&#xe61f;',
		'icon-add-to-list': '&#xe620;',
		'icon-list4': '&#xe621;',
		'icon-ellipsis': '&#xe622;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
