//$(document).ready(function() { $("#global-product-search").select2({
//  minimumInputLength: 2,
//  }); });

$.ajaxSetup({
    statusCode: {
        302: function (response) {
            var redirect_url = response.getResponseHeader('X-Ajax-Redirect-Url');
            if (redirect_url != undefined) {
                window.location.pathname = redirect_url;
            }
        }
    }
});

function equalHeight(group) {    
    var tallest = 0;    
    group.each(function() {       
        var thisHeight = $(this).height();       
        if(thisHeight > tallest) {          
            tallest = thisHeight;       
        }    
    });    
    group.each(function() { $(this).height(tallest); });
} 

/* activate scrollspy menu */
$('body').scrollspy({
  target: '#navbar-collapsible',
  offset: 50
});

/* smooth scrolling sections */
$('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 50
        }, 1000);
        return false;
      }
    }
});

/* http://stackoverflow.com/questions/18023493/bootstrap-3-dropdown-sub-menu-missing/18024991#18024991 */

$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
  // Avoid following the href location when clicking
  event.preventDefault();
  // Avoid having the menu to close when clicking
  event.stopPropagation(); 
  // If a menu is already open we close it
  $('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
  // opening the one you clicked on
  $(this).parent().addClass('open');

  var menu = $(this).parent().find("ul");
  var menupos = $(menu).offset();

  if (menupos.left + menu.width() > $(window).width()) {
    var newpos = -$(menu).width();
    menu.css({ left: newpos });    
  } else {
    var newpos = $(this).parent().width();
    menu.css({ left: newpos });
  }
});

function include(arr, obj) {
  for(var i=0; i<arr.length; i++) {
    if (arr[i] == obj) return true;
  }
}
function getpos(arr, obj) {
  for(var i=0; i<arr.length; i++) {
    if (arr[i] == obj) return i;
  }
}

function angle() { $('#cart-image').css({'-webkit-transform' : 'rotate(0deg)','-moz-transform' : 'rotate(0deg)' });}


(function ($) {
    
    $.fn.slideUpTransition = function() {
        return this.each(function() {
            var $el = $(this);
            $el.css("max-height", "0");
            $el.addClass("height-transition-hidden");
                    
        });
    };

    $.fn.slideDownTransition = function() {
        return this.each(function() {
            var $el = $(this);
            $el.removeClass("height-transition-hidden");

            // temporarily make visible to get the size
            $el.css("max-height", "none");
            var height = $el.outerHeight();

            // reset to 0 then animate with small delay
            $el.css("max-height", "0");

            setTimeout(function() {
                $el.css({
                    "max-height": height
                });
            }, 1);
        });
    };
})(jQuery);

/*$('.thumbnail').draggable({
    revert:true,
    proxy:'clone',
    onStartDrag:function(){
        $(this).draggable('options').cursor = 'not-allowed';
        $(this).draggable('proxy').css('z-index',10);
    },
    onStopDrag:function(){
        $(this).draggable('options').cursor='move';
    }
});

$('#cart').droppable({
    onDragEnter:function(e,source){
        $(source).draggable('options').cursor='auto';
    },
    onDragLeave:function(e,source){
        $(source).draggable('options').cursor='not-allowed';
    },
    onDrop:function(e,source){
        var name = $(source).find('p:eq(0)').html();
        var price = $(source).find('p:eq(1)').html();
        addProduct(name, parseFloat(price.split('$')[1]));
    }
});

var data = {"total":0,"rows":[]};
var totalCost = 0;
function addProduct(name,price){
    function add(){
        for(var i=0; i<data.total; i++){
            var row = data.rows[i];
            if (row.name == name){
                row.quantity += 1;
                return;
            }
        }
        data.total += 1;
        data.rows.push({
            name:name,
            quantity:1,
            price:price
        });
    }
    add();
    totalCost += price;
    $('#cartcontent').datagrid('loadData', data);
    $('div.cart .total').html('Total: $'+totalCost);
}    



Re: Script not running unless I refresh page
Simple answer:

Because of the way JQM is built, the script will ONLY run on the page you have entered from. You have two pages - page 1 and page 2.

The user enters the app from page 1, any scripts in the <head> of page 2 WILL NOT RUN. There are multiple solutions for this.

EDIT: Short answer, <head> only matters for the first page. All other pages the only thing used in the <head> of the page is the title.

1) Add the scripts in the <body> of the page instead of the <head> of the page. This way scripts are loaded page by page

2) Create one .js file that includes the javascript for every page - include this on the top of each page. This solution will allow users to enter from any page and the javascript will be loaded once.

3) If the user has to enter from a certain page, but all the javascript in the <head> of that page.

Note: Because of the way JQM works, everything acts as "one page". This means you need to be careful when using id's. It's suggested to use classes instead. This has nothing to do with what you posted, just a warning as you move forward.

Relevant thread: https://forum.jquery.com/topic/using-ids-in-jquery-mobile-pages
*/

