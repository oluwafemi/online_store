#---
# Excerpted from "Agile Web Development with Rails",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material, 
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose. 
# Visit http://www.pragmaticprogrammer.com/titles/rails4 for more book information.
#---
class OrderNotifier < ActionMailer::Base
  default from: "#{ApplicationHelper::STORE_NAME} <store@#{ApplicationHelper::STORE_ROOT_NAME}>"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.received.subject
  #
  def received(order)
    @order = order

    html = render_to_string('order_notifier/order_invoice.html')
    pdf = Docverter::Conversion.run do |c|
      c.from = 'html'
      c.to = 'pdf'
      c.content = html
    end
    attachments['invoice.pdf'] = pdf

    mail to: order.email, subject: "#{ApplicationHelper::STORE_NAME} Order Confirmation"
  end

  def sale_made(order)
    @order = order

    AdminUser.where("superuser = true").each do |admin_user| 
      mail to: admin_user.email, subject: "#{ApplicationHelper::STORE_NAME} Sale Made"
    end
    
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.shipped.subject
  #
  def shipped(order)
    @order = order

    mail to: order.email, subject: "#{ApplicationHelper::STORE_NAME} Order Shipped"
  end

end
