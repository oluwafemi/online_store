class ProductCategoriesController < ApplicationController
	include CurrentCart
  	before_action :set_cart
  	
	def show
		@product_category = ProductCategory.find(params[:id])
		@products = ProductCategory.paginated_products(params[:id], params[:page])
	end

	def index
		@product_categories = ProductCategory.all #with_sub_categories
	end

	private

		def product_categories_params
			params.require(:product_category).permit(:name)
		end
end
