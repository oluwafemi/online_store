class StoreController < ApplicationController
	#skip_before_action :authorize
  	include CurrentCart
  	before_action :set_cart

  	def index
  		@products = Product.paginate(:page => params[:page], :per_page => 15).order('product_sub_category_id, name')
  	end
end
#https://www.youtube.com/watch?v=ian4nBwQfog   , :per_page => 2

