class ProductSubCategoriesController < ApplicationController
	include CurrentCart
  	before_action :set_cart
  	
	def show
		@product_sub_category = ProductSubCategory.find(params[:id])
		@products = ProductSubCategory.paginated_products(params[:id], params[:page])
	end

	private

		def product_sub_categories_params
			params.require(:product_sub_category).permit(:name, :product_category_id)
		end
end
