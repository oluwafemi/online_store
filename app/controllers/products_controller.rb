class ProductsController < ApplicationController
	include CurrentCart
  	before_action :set_cart
  	
	def show
		@product = Product.find(params[:id])
	end

	def index
		@products = Product.search(params[:search])
	end

	def search_for_products
		@products = Product.select([:id, :name]).where("name ilike :q", q: "%#{params[:q]}%").
			paginate(:page => params[:page], :per_page => params[:per]).order('name')
		# also add the total count to enable infinite scrolling
		products_count = Product.select(:id).where("name ilike :q", q: "%#{params[:q]}%").count

        respond_to do |format|
        	format.json { render json: {total: products_count, products: @products.map { |e| {id: e.id, text: "#{e.name}"} }} }
        end
    end

	private

		def products_params
			params.require(:product).permit(:upc_code, :sku_code, :name, :description, 
            :product_category_id, :product_sub_category_id, :price_kobo, :start_balance, :balance, :img_url, :image)
		end
end
