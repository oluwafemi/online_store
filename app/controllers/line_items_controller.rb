class LineItemsController < ApplicationController
	#skip_before_action :authorize, only: :create
  #respond_to :html, :js
 	include CurrentCart
 	before_action :set_cart, only: [:create, :destroy]
 	before_action :set_line_item, only: [:show, :edit, :update, :destroy]

 	# GET /line_items
 	# GET /line_items.json
 	def index
 		@line_items = LineItem.paginate(:page => params[:page]).order('id')
 	end

 	# GET /line_items/1
 	# GET /line_items/1.json
 	def show
 	end

  	# GET /line_items/new
  	def new
    	@line_item = LineItem.new
  	end

	# GET /line_items/1/edit
  	def edit
  	end

  	# POST /line_items
  	# POST /line_items.json
  	def create
		  @line_item = @cart.add_product(params[:product_id])
		  respond_to do |format|
        if @line_item.save
          format.html { redirect_to store_url }
        	format.js   { @current_item = @line_item }
        	format.json { render action: 'show', status: :created, location: @line_item }
			  else
				  format.html { render :action => "new" } 
				  format.json { render json: @line_item.errors, status: :unprocessable_entity }
			  end
		  end
	  end

	# PATCH/PUT /line_items/1
  	# PATCH/PUT /line_items/1.json
  	def update
  		respond_to do |format|
  			if @line_item.update(line_item_params)
  				format.html { redirect_to @line_item, notice: 'Line item was successfully updated.' }
  				format.json { head :no_content }
  			else
  				format.html { render action: 'edit' }
  				format.json { render json: @line_item.errors, status: :unprocessable_entity }
  			end
  		end
  	end

  	# DELETE /line_items/1
  	# DELETE /line_items/1.json
  	def destroy
      update_line_item
      update_cart
      respond_to do |format|
        format.html do 
          redirect_to (:back)
        end
        format.js  do
          if should_go_back_to_store
            head 302, x_ajax_redirect_url: store_path
          else
            render :destroy
          end
        end
        format.json { head :no_content }
      end
  	end

  	private
    	# Use callbacks to share common setup or constraints between actions.
    	def set_line_item
      		@line_item = LineItem.find(params[:id])
    	end

      def update_line_item
        @line_item.quantity -= 1
        if @line_item.quantity == 0
          @line_item.destroy
        else
          @line_item.save
        end
      end

      def update_cart
        if @cart.line_items.empty?
          @cart.destroy
          session[:cart_id] = nil
        end
      end

      def should_go_back_to_store
        (session[:cart_id] == nil) and (request.referrer.include? new_order_url)
      end

      # Never trust parameters from the scary internet, only allow the white
    	# list through.
    	def line_item_params
      		params.require(:line_item).permit(:product_id)
    	end
end
