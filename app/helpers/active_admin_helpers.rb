module ActiveAdminHelpers
	#include ApplicationHelper

	# make this method public (compulsory)
 	def self.included(dsl)
    	# nothing ...
 	end

	# define helper methods here ...
	def administrator?(admin_user)
    	admin_user.full_name == ApplicationHelper::STORE_ADMINISTRATOR or 
    		admin_user.full_name == ApplicationHelper::APP_ADMINISTRATOR
  	end 
end