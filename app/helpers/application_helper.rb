module ApplicationHelper
	def remote_request(type, path, params={}, target_tag_id)
    "$.#{type}('#{path}',{#{params.collect { |p| "#{p[0]}: #{p[1]}" }.join(", ")}}, function(data) {$('##{target_tag_id}').html(data);} );"
  end

  STORE_NAME = "QPM Stationery"

  SHORT_STORE_NAME = "QPM"

  STORE_DOMAIN_NAME = "www.qpmstationery.com" #"radiant-earth-1929.herokuapp.com"

  STORE_ROOT_NAME = "qpmstationery.com"

  STORE_ADMINISTRATOR = "Store Admin Administrator"

  APP_ADMINISTRATOR = "App Admin Administrator"
    
  def hidden_div_if(condition, attributes = {}, &block)
   	if condition
   		attributes["style"] = "display: none"
   	end
    	content_tag("div", attributes, &block)
  end

end