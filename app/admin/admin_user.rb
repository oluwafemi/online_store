include ActiveAdminHelpers

ActiveAdmin.register AdminUser do
  permit_params :first_name, :last_name, :email, :superuser, :password, :password_confirmation
  actions :all
  config.clear_action_items!
  scope_to :current_admin_user, unless: proc{ current_admin_user.superuser? }

  action_item only: [:show, :index] do
    links = link_to "New Admin User", new_admin_admin_user_path
  end

  show :title => :current_title do |admin_user|
    attributes_table do
      if admin_user.encrypted_password == ''
        row :first_name
        row :last_name
        row :email
      else
        row 'First Name' do 
          text = "<a href='#{edit_admin_admin_user_path(admin_user.id)}'>#{admin_user.first_name}</a>"
          text.html_safe
        end

        row 'Last Name' do 
          text = "<a href='#{edit_admin_admin_user_path(admin_user.id)}'>#{admin_user.last_name}</a>"
          text.html_safe
        end

        row 'Email Address' do 
          text = "<a href='#{edit_admin_admin_user_path(admin_user.id)}'>#{admin_user.email}</a>"
          text.html_safe
        end
      end      
      row :sign_in_count
      row :current_sign_in_ip
      row :last_sign_in_ip
      row :created_at
      row :updated_at
      row :superuser
    end
    active_admin_comments
  end

  controller do
    
    def action_methods
      if current_admin_user.superuser
        super
      end
    end

  end

  menu if: proc{ current_admin_user.superuser }

  index do
    column "Admin User Name" do  |admin_user|
      link_to admin_user.full_name, admin_admin_user_path(admin_user.id)
    end
    column "Email Address" do  |admin_user|
      link_to admin_user.email, admin_admin_user_path(admin_user.id)
    end
    column :current_sign_in_at
    column :last_sign_in_at
    column :sign_in_count
    column :superuser
    column do |admin_user|
      if admin_user.encrypted_password == ''
        delete = (current_admin_user == admin_user) ? "" :
          " | " + link_to("Delete", admin_admin_user_path(admin_user), :method => :delete, :confirm => "Are you sure?") 
      
        link_to("Details", admin_admin_user_path(admin_user)) + " | Edit" + delete.try(:html_safe)
      else
        delete = ((current_admin_user == admin_user) or (administrator?(admin_user))) ? "" :
        " | " + link_to("Delete", admin_admin_user_path(admin_user), :method => :delete, :confirm => "Are you sure?")
      
        link_to("Details", admin_admin_user_path(admin_user)) + " | " + \
        link_to("Edit", edit_admin_admin_user_path(admin_user)) + delete.try(:html_safe)
      end
    end
  end

  filter :email
  filter :superuser
  filter :current_sign_in_at
  filter :last_sign_in_at
  filter :sign_in_count

  form do |f|
    f.inputs "Admin Details" do
      if f.object.persisted?
        if (not administrator?(f.object))
          f.input :first_name
          f.input :last_name
        end     
      else
        f.input :first_name
        f.input :last_name
      end
      f.input :email
      if f.object.persisted?
        if (current_admin_user != f.object) and (not administrator?(f.object))
          f.input :superuser, :label => "Super User Priveleges" 
        end     
      else
        f.input :superuser, :label => "Super User Priveleges" 
      end
    end
    f.actions
  end

end