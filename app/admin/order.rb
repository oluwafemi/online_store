ActiveAdmin.register Order do
  actions :all, :except => [:new, :destroy]
  config.sort_order = "updated_at"

  menu if: proc{ current_admin_user.superuser }
  
  show :title => :name do |order|
    attributes_table do 
      row :customer_name
      row :address
      row :email
      row :pay_type
      row :state
      row 'Ordered Products' do
        order.line_items.map do |line_item| 
          "<a href='#{admin_product_path(line_item.product)}'>#{line_item.product_name}</a>  \
          (#{line_item.quantity}  X  #{line_item.formatted_total_price})  =  \
          #{line_item.formatted_unit_price}"
        end.join("<br /><br />").html_safe
      end
      row :order_amount     
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  index do
    column :customer_name
    column :customer_email
    column :order_amount
    column 'Order State' do |order|
      text = "<a href='#{admin_order_path(order)}'>#{order.state}</a>";
      text.html_safe
    end
    column :created_at
    column :updated_at
  end

  filter :name
  filter :created_at
  filter :updated_at

end