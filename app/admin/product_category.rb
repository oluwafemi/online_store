ActiveAdmin.register ProductCategory do
  actions :all
  config.clear_action_items!
  config.sort_order = "name_asc"
  permit_params :name

  controller do
    
    def action_methods
      if current_admin_user.superuser
        super
      end
    end

    def change_sub_categories
      if (params[:product_category_id] == "") 
        @sub_categories = []
      else
        @sub_categories = ProductCategory.product_sub_categories(params[:product_category_id])
      end
      render :text => view_context.options_from_collection_for_select(@sub_categories, :id, :name)
    end
  end

  menu :if => proc{ current_admin_user.superuser }

  action_item only: [:show, :index] do
    link_to "New Product Category", new_admin_product_category_path
  end
  
  show :title => :name do |product_category|
    attributes_table do 
      row 'Category Name' do
        text = "<a href='#{edit_admin_product_category_path(product_category)}'>#{product_category.name}</a>";
        text.html_safe
      end
      row 'Sub Categories' do
        product_category.product_sub_categories.map { |sub_category| "<a href='#{admin_product_sub_category_path(sub_category)}'>#{sub_category.name}</a>" }.join("<br /><br />").html_safe
      end
    end
    active_admin_comments
  end

  index do
    column "Category Name" do  |product_category|
      link_to product_category.name, admin_product_category_path(product_category)
    end
    column do |product_category|
      if product_category.product_sub_categories.any?
        link_to("Details", admin_product_category_path(product_category)) + " | " + \
        link_to("Edit", edit_product_category_path(product_category))
      else
        link_to("Details", admin_product_category_path(product_category)) + " | " + \
        link_to("Edit", edit_product_category_path(product_category)) + " | " + \
        link_to("Delete", admin_product_category_path(product_category), :method => :delete, :confirm => "Are you sure?") 
      end      
    end
  end

  form do |f|
    f.inputs "Product Category Details" do
      f.input :name
    end
    f.actions
  end
  
end

#https://www.youtube.com/watch?v=_5qryIjyQJE
