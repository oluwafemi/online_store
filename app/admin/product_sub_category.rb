ActiveAdmin.register ProductSubCategory do
  actions :all
  config.clear_action_items!
  config.sort_order = "name_asc"
  permit_params :name, :product_category_id

  index do
    column "Sub Category Name" do  |sub_category|
      link_to sub_category.name, admin_product_sub_category_path(sub_category)
    end
    column :product_category
    column do |sub_category|
      if sub_category.products.any?
        link_to("Details", admin_product_sub_category_path(sub_category)) + " | " + \
        link_to("Edit", edit_admin_product_sub_category_path(sub_category))
      else
        link_to("Details", admin_product_sub_category_path(sub_category)) + " | " + \
        link_to("Edit", edit_admin_product_sub_category_path(sub_category))
        link_to("Delete", admin_product_sub_category_path(sub_category), :method => :delete, :confirm => "Are you sure?") 
      end
    end
  end

  menu :if => proc{ current_admin_user.superuser }

  action_item only: [:show, :index] do
    link_to "New Product Sub Category", new_admin_product_sub_category_path
  end

  show :title => :name do |sub_category|
    attributes_table do
      row 'Sub Category Name' do
        text = "<a href='#{edit_admin_product_sub_category_path(sub_category)}'>#{sub_category.name}</a>";
        text.html_safe
      end
      row 'Product Category' do
        text = "<a href='#{admin_product_category_path(sub_category.product_category_id)}'>#{sub_category.product_category_name}</a>";
        text.html_safe
      end 
      row :created_at
    end
    active_admin_comments
  end
end