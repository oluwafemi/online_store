ActiveAdmin.register Product do
  actions :all
  config.clear_action_items!
  config.sort_order = "name_asc"
  form :partial => "form"
  permit_params :upc_code, :sku_code, :name, :description, :product_category_id, 
    :product_sub_category_id, :price_cents, :start_balance, 
    :balance, :img_url, :image, :image_cache

  controller do

    def normalize_fields_in_params
      price_in_float = params[:product][:product_price_without_symbol].remove(',').to_f
      price_in_int = (price_in_float.round(2) * 100).round
      params[:product][:price_cents] = "#{price_in_int}"

      params[:product][:balance] = "#{params[:product][:balance].to_i}"
    end

    def create
      normalize_fields_in_params
      super
    end

    def update
      normalize_fields_in_params
      super
    end

  end

  action_item only: [:show, :index] do
    links = link_to "New Product", new_admin_product_path
    #links += link_to "Edit Product", edit_resource_path(resource)
  end
  
  show :title => :name do |product|
    attributes_table do 
      row 'Name' do 
        text = "<a href='#{edit_admin_product_path(product.id)}'>#{product.name}</a>"
        text.html_safe
      end
      row :upc_code
      row :sku_code
      row :product_price
      row :balance
      row 'Category' do 
        text = "<a href='#{admin_product_category_path(product.product_category_id)}'>#{product.category_name}</a>"
        text.html_safe
      end
      row 'Sub Category' do
        text = "<a href='#{admin_product_sub_category_path(product.product_category_id)}'>#{product.sub_category_name}</a>"
        text.html_safe
      end
      row :image do
        image_tag(product.image.to_s)
      end
      row "Product Description" do
        sanitize product.description
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  index do
    column "Name" do  |product|
      link_to product.name, admin_product_path(product.id)
    end
    column :upc_code
    column :sku_code
    #column :description
    column :product_price
    column "Product Category" do  |product|
      link_to product.category_name, admin_product_category_path(product.product_category_id)
    end
    column "Product Sub Category" do  |product|
      link_to product.sub_category_name, admin_product_sub_category_path(product.product_sub_category_id)
    end
    column do  |product|
      if product.line_items.any?
        link_to("Edit", edit_admin_product_path(product))
      else
        link_to("Edit", edit_admin_product_path(product)) + " | " + \
        link_to("Delete", admin_product_path(product), :method => :delete, :confirm => "Are you sure?") 
      end      
    end
  end

  filter :name
  filter :created_at
  filter :updated_at
  
end