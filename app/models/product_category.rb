class ProductCategory < ActiveRecord::Base
  default_scope { order("name") }
  has_many :product_sub_categories
	has_many :products, :through => :product_sub_categories
  has_many :child_products, class_name:  "Product",
                                  foreign_key: "product_category_id",
                                  dependent:   :destroy

  accepts_nested_attributes_for :product_sub_categories, :allow_destroy => true

  before_validation :normalize_name, on: [:create, :update]

  before_destroy :ensure_not_referenced_by_any_product_sub_category
 
  validates :name, presence: true, uniqueness: true

  def self.active_product_categories
   	all
  end

  def self.paginated_products(id, page)
    find(id).child_products.paginate(:page => page, :per_page => 15).order('name')
  end

  def self.with_sub_categories
    all.select { |product_category| not product_category.product_sub_categories.empty? }
  end

  def self.product_sub_categories(instance_id)
    find(instance_id).product_sub_categories
  end

  def ensure_not_referenced_by_any_product_sub_category
    if product_sub_categories.any?
      errors[:base] << "Product Sub Categories present"
      return false
    end
    return true
  end

  protected
    def normalize_name
      self.name = self.name.upcase
    end

  private

    def permitted_params
      params.require(:product_category).permit(:name)
    end

end


