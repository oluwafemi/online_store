class Order < ActiveRecord::Base
  include AASM
  #PAYMENT_TYPES = [ "Check", "Credit card", "Purchase order" ]
  MONTHS = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']

  attr_accessor :card_number, :expiry_month, :expiry_year, :cvv
  
  has_many :line_items, dependent: :destroy
  has_many :products, :through => :line_items
  has_paper_trail

  validates :name, :address, :email, :card_number, :expiry_month, :expiry_year, :cvv, presence: true
  validates :cvv, numericality: { only_integer: true }
  validates :expiry_month, inclusion: MONTHS
  #validates :pay_type, inclusion: PAYMENT_TYPES

  monetize :total_price_cents, :as => "total_price"

  accepts_nested_attributes_for :line_items, :allow_destroy => true

  aasm column: 'state' do
    state :pending, initial: true
    state :processing
    state :finished
    state :errored
    
    event :process, after: :charge_card do
      transitions from: :pending, to: :processing
    end

    event :finish do
      transitions from: :processing, to: :finished
    end

    event :fail do
      transitions from: :processing, to: :errored
    end 
  end

  
  def add_line_items_from_cart(cart)
    self.total_price_cents = cart.total_price_cents
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end

  def customer_name
    name
  end

  def customer_email
    email
  end

  def formatted_total_price
    self.total_price.format(:no_cents_if_whole => false, :symbol => true)
  end

  def order_amount
    formatted_total_price
  end

  def total_quantities
    line_items.to_a.sum { |item| item.quantity }
  end

  private

    def permitted_params
      params.require(:order).permit(:name, :address, :email, :pay_type, :card_number, 
        :expiry_month, :expiry_year, :cvv)
    end

    def charge_card
      #begin
        save!
        #charge = Stripe::Charge.create(
        #  amount: self.amount,
        #  currency: "usd",
        #  card: self.stripe_token,
        #  description: self.email
        #)
        #balance = Stripe::BalanceTransaction.retrieve(charge.balance_transaction)
        #self.update(
        #  stripe_id: charge.id,
        #  card_expiration: Date.new(charge.card.exp_year, charge.card.exp_month, 1),
        #  fee_amount: balance.fee
        #)
        self.finish!
      #rescue Stripe::StripeError => e
      #  self.update_attributes(error: e.message)
      #  self.fail!
      #end
    end
end
#ssl_requirement
#active_merchant
#acts_as_list
#acts_as_tree
#annotate_models
#asset_packager
#attachment_fu
#authlogic
#auto_complete
#calendar_date_select
#clearance
#delayed_job