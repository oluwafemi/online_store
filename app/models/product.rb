class Product < ActiveRecord::Base
    default_scope { order("name") }
    belongs_to :product_category
    belongs_to :product_sub_category

    has_many :line_items
    has_many :orders, :through => :line_items

    monetize :price_cents, :as => "price", :numericality => {
        :greater_than => 0
    }

    before_validation :normalize_fields, on: [:create, :update]

    after_validation :reset_description, on: [:create, :update]

    before_destroy :ensure_not_referenced_by_any_line_item

    validates :upc_code, :presence => true, uniqueness: true
    validates :description, :product_category_id, :product_sub_category_id, :price_cents, :image, :presence => true
    validates :name, presence: true, uniqueness: { scope: :product_sub_category_id, message: "already exist in this sub-category" }
    validates :description, length: {
        minimum: 1,
        maximum: 20,
        tokenizer: lambda { |str| str.scan(/\w+/) },
        too_short: "must have at least %{count} words",
        too_long: "must have at most %{count} words"
    }

    mount_uploader :image, ImageUploader

    #alias_attribute :entry_name, :name

    def sub_category_name
        product_sub_category.name
    end

    def category_name
        product_category.name
    end
  
    def self.order_by_name
        order('name ASC')
    end

    def product_sibling_sub_categories
        product_category.product_sub_categories
    end

    def product_price
        self.price.format(:no_cents_if_whole => false, :symbol => true)
        #humanized_money_with_symbol self.price
    end

    def product_price_without_symbol
        self.price_cents = 0 if self.price_cents.nil?            
        self.price.format(:no_cents_if_whole => false, :symbol => false)
        #humanized_money_with_symbol self.price
    end

    def ensure_not_referenced_by_any_line_item
        if line_items.any?
            errors[:base] << "Line Items present"
            return false
        end
        return true
    end

    def self.search(search)
        if search
            find(:all, :conditions => ['name LIKE ?', "%#{search}%"])
        else
            find(:all)
        end
    end

    # paginate :page => params[:page], :order => 'name'

    protected
    
      def normalize_fields
        self.name = self.name.downcase.titleize
        
        self.description = self.description.downcase.titleize
        @sanitized_description = self.description

        self.description = ApplicationController.helpers.strip_tags(self.description)
        
        self.upc_code = self.upc_code.upcase
        if self.sku_code.empty?
            self.sku_code = nil
        else
            self.sku_code = self.sku_code.upcase
        end      
      end

      def reset_description
        self.description = @sanitized_description     
      end

    private 

      def permitted_params
        params.require(:product).permit(:upc_code, :sku_code, :name, :description, 
            :product_category_id, :product_sub_category_id, :price_cents, :start_balance, :balance, :img_url, :image, :image_cache)
      end
end
