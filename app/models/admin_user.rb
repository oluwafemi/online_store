class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  after_create { |admin| admin.send_reset_password_instructions }

  before_validation :normalize_full_names, on: [:create, :update]

  after_destroy :ensure_an_admin_remains

  before_save :set_superuser_field

  validates :last_name, :email, presence: true

  validates :first_name, presence: true, uniqueness: { scope: :last_name, message: "Admin user name already exist" }

  def admin_user_params 
    params.require(:admin_user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :remember_me, :superuser)
  end
  
  def password_required?
    new_record? ? false : super
  end

  def current_title
    email
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  protected
    def normalize_full_names
      self.first_name = self.first_name.downcase.titleize
      self.last_name = self.last_name.downcase.titleize
    end

    def set_superuser_field
      if self.full_name == ApplicationHelper::STORE_ADMINISTRATOR or 
        self.full_name == ApplicationHelper::APP_ADMINISTRATOR
        self.superuser = true
      end
    end

  private
    def ensure_an_admin_remains
      if AdminUser.count.zero?
        raise "Can't delete last user"
      end
    end
end
