class ProductSubCategory < ActiveRecord::Base
	belongs_to :product_category
  has_many :products
    
  validates :name, presence: true, uniqueness: { scope: :product_category_id, message: "already exist in this category" }

  validates :product_category_id, presence: true
    
  before_validation :normalize_name, on: [:create, :update]

  before_destroy :ensure_not_referenced_by_any_product

  def product_category_name
    product_category.name
  end

  # find returns #<ActiveRecord::Associations::CollectionProxy 
  #   can be queried for association methods
  # where returns #<ActiveRecord::Relation

  def self.paginated_products(id, page)
    find(id).products.paginate(:page => page, :per_page => 15).order('name')
  end

  def ensure_not_referenced_by_any_product
    if products.any?
      errors[:base] << "Products present"
      return false
    end
    return true
  end

  protected

  	def normalize_name
      self.name = self.name.upcase
    end

  private

  	def permitted_params
    	params.require(:product_sub_category).permit(:name, :product_category_id)
    end
end
