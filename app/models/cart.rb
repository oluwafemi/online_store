class Cart < ActiveRecord::Base
	has_many :line_items, :dependent => :destroy

	def add_product(product_id)
		current_item = line_items.find_by(product_id: product_id)
		if current_item
			current_item.quantity += 1
		else
			current_item = line_items.build(product_id: product_id)
			current_item.price_cents = current_item.product.price_cents
		end
		current_item
	end

	def total_price
		line_items.to_a.sum { |item| item.total_price }
	end

	def total_price_cents
		line_items.to_a.sum { |item| item.total_price_cents }
	end

	def formatted_total_price
        Money.new(total_price).format(:no_cents_if_whole => false, :symbol => true)
    end

    def formatted_est_shipping
        Money.new(0).format(:no_cents_if_whole => false, :symbol => true)
    end
end