class LineItem < ActiveRecord::Base
	#default_scope { order("id") }
	belongs_to :product
    belongs_to :cart
    belongs_to :order

    monetize :price_cents, :as => "price", :numericality => {
        :greater_than => 0
    }

    validates :product_id, :price_cents, :presence => true
    #validates :product_id, presence: true, uniqueness: { scope: :cart_id, message: "already exist in this cart" }
    validates :quantity, numericality: { greater_than: 0 }

    def product_name
        product.name
    end

    def product_image
        product.image
    end

    def total_price
    	price * quantity
    end

    def total_price_cents
        price_cents * quantity
    end

    def formatted_total_price
        total_price.format(:no_cents_if_whole => false, :symbol => true)
    end

    def formatted_unit_price
        price.format(:no_cents_if_whole => false, :symbol => true)
    end

    private 

      def permitted_params
        params.require(:product).permit(:product_id, :cart_id, :order_id, :price_cents, :quantity)
      end
end