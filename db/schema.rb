# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141029014845) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "superuser",              default: false, null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["first_name", "last_name"], name: "admin_user_first_last_name_idx", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "line_items", force: true do |t|
    t.integer  "product_id",              null: false
    t.integer  "cart_id"
    t.integer  "price_cents"
    t.integer  "quantity",    default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order_id"
  end

  add_index "line_items", ["product_id", "cart_id"], name: "product_cart_idx", unique: true, using: :btree
  add_index "line_items", ["product_id", "order_id"], name: "product_order_idx", unique: true, using: :btree

  create_table "orders", force: true do |t|
    t.string   "name"
    t.text     "address"
    t.string   "email"
    t.string   "pay_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "total_price_cents"
    t.string   "state"
    t.string   "error"
  end

  create_table "product_categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_categories", ["name"], name: "category_name_idx", unique: true, using: :btree

  create_table "product_sub_categories", force: true do |t|
    t.string   "name"
    t.integer  "product_category_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_sub_categories", ["name", "product_category_id"], name: "sub_category_idx", unique: true, using: :btree

  create_table "products", force: true do |t|
    t.string   "upc_code"
    t.string   "sku_code"
    t.string   "name"
    t.text     "description"
    t.integer  "product_sub_category_id", null: false
    t.integer  "price_cents"
    t.integer  "start_balance"
    t.integer  "balance"
    t.string   "img_url"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_category_id",     null: false
  end

  add_index "products", ["name", "product_sub_category_id"], name: "product_name_idx", unique: true, using: :btree
  add_index "products", ["sku_code"], name: "sku_code_idx", unique: true, using: :btree
  add_index "products", ["upc_code"], name: "upc_code_idx", unique: true, using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  add_foreign_key "line_items", "carts", name: "line_items_cart_id_fk", dependent: :delete
  add_foreign_key "line_items", "orders", name: "line_items_order_id_fk", dependent: :delete
  add_foreign_key "line_items", "products", name: "line_items_product_id_fk", dependent: :delete

  add_foreign_key "product_sub_categories", "product_categories", name: "product_sub_categories_product_category_id_fk", dependent: :delete

  add_foreign_key "products", "product_categories", name: "products_product_category_id_fk", dependent: :delete
  add_foreign_key "products", "product_sub_categories", name: "products_product_sub_category_id_fk", dependent: :delete

end
