class AddOrderIdToLineItem < ActiveRecord::Migration
  def change
  	change_table :line_items do |t|
      t.references :order, :null => true
  	  t.foreign_key :orders, dependent: :delete
    end
    add_index :line_items, [:product_id, :order_id], :unique => true, :name => 'product_order_idx'
  end
end
