class ChangeProductsColumnName < ActiveRecord::Migration
  def change
  	rename_column :products, :price_kobo, :price_cents
  end
end
