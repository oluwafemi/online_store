class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :upc_code
      t.string :sku_code # is unique unique to a retailer
      t.string :name
      t.text :description
      t.references :product_sub_category, :null => false
      t.integer :price_kobo
      t.integer :start_balance, :default => 0
      t.integer :balance, :default => 0
      t.string :img_url
      t.string :image

      t.timestamps

      t.foreign_key :product_sub_categories, dependent: :delete
    end
    add_index :products, [:upc_code], :unique => true, :name => 'upc_code_idx'
    add_index :products, [:sku_code], :unique => true, :name => 'sku_code_idx'
    add_index :products, [:name, :product_sub_category_id], :unique => true, :name => 'product_name_idx'
  end
end