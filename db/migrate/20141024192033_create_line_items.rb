class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.references :product, :null => false
      t.references :cart, :null => true
      t.integer :price_cents
      t.integer :quantity, :default => 1
      t.timestamps
      t.foreign_key :products, dependent: :delete
      t.foreign_key :carts, dependent: :delete
    end
    add_index :line_items, [:product_id, :cart_id], :unique => true, :name => 'product_cart_idx'
  end
end
