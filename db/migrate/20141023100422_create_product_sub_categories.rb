class CreateProductSubCategories < ActiveRecord::Migration
  def change
    create_table :product_sub_categories do |t|
      t.string :name
      t.references :product_category, :null => false
      t.timestamps
      t.foreign_key :product_categories, dependent: :delete
    end
    add_index :product_sub_categories, [:name, :product_category_id], :unique => true, :name => 'sub_category_idx'
  end
end
