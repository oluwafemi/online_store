class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.text :address
      t.string :email
      t.string :pay_type
      t.timestamps
    end
  end
end

#t.references customer
#guid:string \
#stripe_id:string
#ship_date

# we ssl-enable our site
# we setup account with a payment gateway, open a merchant account etc
# we ssl send payment details from browser to our app server
# or send it directly from the client-side with javascript, whichever is best practices
# we must never save these information to our database 
# (because we wont be guaranteeing PCI compliance, we pass that resposibility to our payment gateway)
# we send it at the controller level (perhaps through validate, :send_details_to_gateway, :on => :create) to our payment gateway / payment provider (stripe / cardstream etc)
# we only persist what encrypted payment ids the payment provider returns