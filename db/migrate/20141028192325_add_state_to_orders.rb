class AddStateToOrders < ActiveRecord::Migration
  def change
  	change_table :orders do |t|
      t.string :state
      t.string :error
    end
  end
end
