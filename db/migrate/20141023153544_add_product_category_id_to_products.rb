class AddProductCategoryIdToProducts < ActiveRecord::Migration
  def change
  	change_table :products do |t|
  	  t.references :product_category, :null => false
      t.foreign_key :product_categories, dependent: :delete
    end
  end
end
