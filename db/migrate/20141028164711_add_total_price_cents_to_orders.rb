class AddTotalPriceCentsToOrders < ActiveRecord::Migration
  def change
  	change_table :orders do |t|
      t.integer :total_price_cents
    end
  end
end
