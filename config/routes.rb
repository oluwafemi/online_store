Rails.application.routes.draw do
  
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  get 'static_pages/home'

  get "store/index"

  resources :carts, :line_items

  resources :orders, only: [:new, :create]

  root :to => 'store#index', :as => 'store'

  get '/products_search', to: 'products#search_for_products', as: 'products_search'

  post '/admin/products/:product_id/change_sub_categories', :to => 'admin/product_categories#change_sub_categories'
  post '/admin/products/change_sub_categories', :to => 'admin/product_categories#change_sub_categories'
  post '/admin/change_sub_categories', :to => 'admin/product_categories#change_sub_categories'

  resources :products, only: [:index, :show]

  resources :product_sub_categories, only: [:show]

  resources :product_categories, only: [:show]

  resources :product_categories do
    resources :product_sub_categories do
      resources :products, only: [:index, :show]
    end
  end

end